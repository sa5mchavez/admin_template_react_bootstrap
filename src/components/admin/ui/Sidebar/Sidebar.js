import React from "react";
import './styles.css';
// import { Container, Row, Col, Image } from "react-bootstrap";
import { Nav, NavDropdown, Image } from "react-bootstrap";

const Sidebar = ({active}) => {
  
  return (
    <>
      <nav className={`sidebar ${active}`}>
            <div className="sidebar-header">
              <Image 
                  src="https://cdn2.iconfinder.com/data/icons/nodejs-1/512/nodejs-512.png"
                  roundedCircle
                  style={{"height":"70px","width":"70px"}} 
                />
            </div>
            <div className="components">
                <p className="app-title">React App</p>
                <NavDropdown title="Menu" id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Menu 1.2</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="#action/3.4">Menu 1.3</NavDropdown.Item>
                </NavDropdown>

                <Nav.Link>Link</Nav.Link>
                
                <NavDropdown title="Menu 2" id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Menu 2.1</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="#action/3.4">Menu 2.2</NavDropdown.Item>
                </NavDropdown>
                <Nav.Link>Otro link</Nav.Link>
                <NavDropdown title="Menu 3" id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Menu 3.1</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="#action/3.4">Menu 3.2</NavDropdown.Item>
                </NavDropdown>

            </div>
      </nav>
    </>
  );
};


export default Sidebar;