import React from "react";
import './styles.css';
// import { Container, Row, Col, Image } from "react-bootstrap";
// import { Nav, NavDropdown } from "react-bootstrap";
import MenuBar from '../MenuBar';

const PrincipalContainer = ({active, setActive}) => {
  
  return (
    <>
      <div className={`d-none d-sm-block d-block d-md-block d-lg-none content-menu ${active}`} >
        <MenuBar active={active} setActive={setActive}/>
      </div>
      <div className={`content-principal ${active}`} >
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
    </>
    
  );
};


export default PrincipalContainer;