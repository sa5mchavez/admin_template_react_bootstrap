import React, { useState } from "react";
import './styles.css';
import { Container } from "react-bootstrap";
import Sidebar from './ui/Sidebar';
import PrincipalContainer from "./ui/PrincipalContainer";

const AdminUI = ({token}) => {
  
  const [active, setActive] = useState('')

  return (
    <>
        <Container className={"wrapper"}>
            <Sidebar active={active} />
            <PrincipalContainer active={active}  setActive={setActive} />
        </Container>        
    </>
  );
};

export default AdminUI;